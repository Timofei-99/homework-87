import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Grid, Typography} from "@material-ui/core";
import {fetchSoloPosts} from "../../store/actions/postActions";
import {apiURL} from "../../config";
import Comments from "../Comments/Comments";
import CommentForm from "../Comments/CommentForm";

const Post = ({match, image}) => {
    const dispatch = useDispatch();
    const post = useSelector((state) => state.posts.soloPost);
    const user = useSelector((state) => state.users.user);

    useEffect(() => {
        dispatch(fetchSoloPosts(match.params.id));
    }, [dispatch, match.params.id]);

    return (
        <>
            {post ? (
                <Grid container direction="column">
                    <Typography variant="h3">{post.author.username}</Typography>
                    <Typography variant="h5">
                        Post creation date {post.datetime}
                    </Typography>
                    {post.image ? (
                        <img
                            src={apiURL + "/" + post.image}
                            alt={image}
                            style={{width: "600px", margin: "20px 0"}}
                        />
                    ) : null}
                    <p style={{fontSize: "22px"}}>{post.description}</p>
                </Grid>
            ) : null}
            {user ? <CommentForm post={match.params.id}/> : null}
            <Comments id={match.params.id}/>
        </>
    );
};

export default Post;
