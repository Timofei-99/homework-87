const express = require('express');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const body = req.body;

        const userData = {
            username: body.username,
            password: body.password
        };

        const userBody = new User(userData);

        userBody.generateToken();
        await userBody.save();
        res.send(userBody);
    } catch (e) {
        res.status(400).send(e)
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(401).send({message: 'Credentials are wrong!'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(401).send({message: 'Credentials are worng!'});
    }

    user.generateToken();
    await user.save({validateBeforeSave: false});

    res.send({message: 'Username and password correct!', user});
});

module.exports = router;