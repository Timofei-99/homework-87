const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema({
    author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
    },
    post: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Post",
    },
    text: String,
});

const Comment = mongoose.model("Comment", CommentSchema);

module.exports = Comment;
