import {Button, TextField} from "@material-ui/core";
import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {postComment} from "../../store/actions/commentActions";

const CommentForm = ({post}) => {
    const dispatch = useDispatch();
    const [comment, setComment] = useState({
        text: "",
    });

    const inputChangeHandler = (e) => {
        const {name, value} = e.target;

        setComment((prev) => ({...prev, [name]: value}));
    };

    const submitFormHandler = (e) => {
        e.preventDefault();
        dispatch(postComment({...comment, post}));
    };

    return (
        <form onSubmit={submitFormHandler}>
            <TextField
                type="text"
                name="text"
                label="Comment"
                value={comment.text}
                onChange={inputChangeHandler}
                multiline
                minRows={2}
                maxRows={4}
                style={{marginBottom: "20px"}}
            />
            <Button type="submit" variant="outlined" color="secondary">
                send
            </Button>
        </form>
    );
};

export default CommentForm;
