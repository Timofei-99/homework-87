import {Card, CardContent, CardHeader, CardMedia, Grid, makeStyles} from "@material-ui/core";
import React from "react";
import {Link} from "react-router-dom";
import imageNotAvailable from "../../assets/images/3672c2.jpeg";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    card: {
        height: "auto",
    },
    cardHeader: {
        background: "#ff8880",
    },
    media: {
        margin: "15px",
        width: "30%",
        padding: "8%",
    },
});

const PostItem = ({title, image, id, datetime, author}) => {
    const classes = useStyles();

    let cardImage = imageNotAvailable;

    if (image) {
        cardImage = apiURL + "/" + image;
    }

    return (
        <Grid item component={Link} to={"/post/" + id} style={{textDecoration: "none", marginBottom: "50px"}}>
            <Card className={classes.card}>
                <CardHeader title={title} className={classes.cardHeader}/>
                <Grid item container justifyContent="space-between" alignItems="center">
                    <CardMedia image={cardImage} title={title} className={classes.media}/>
                    <CardContent>
                        <p> Date of creation : {datetime}</p>
                        <p> Author: {author}</p>
                    </CardContent>
                </Grid>
            </Card>
        </Grid>
    );
};

export default PostItem;
